/* Globle variables  */
var vstlist = [];
var userData = getUserDetail();
var userObjectJS = JSON.parse(userData);
var mobile = userObjectJS[0].mobile;
var name = userObjectJS[0].firstName;

/* General use functions */

/*1. Function Name : - HallsVisitorList()
   Paremeters    : - No
   Perpose       : - Fatch VST  data     
*/

function HallsVisitorList() {
    var list = getVisitorList();
    /*document.wr("Vstlist"+list);*/
    var userList = JSON.parse(list);
    for (var i = 0; i < userList.length; i++) {
        
        if (i == 0) {

            $("#visitor_list")
                .html('<div class="mcase-wrapper" onclick="createChatEntry(\'' + userList[i].id + '\',\'' + userList[i].firstname + '\');"><div style="height: 50px; position: relative;"><div class="mprice-wrapper" ><span></span></div><div class="morder-wrapper"><div class="order"><span > ' + userList[i].firstname + ' ' + userList[i].lastname + ' </span></div></div><div class="mbottom-line">&nbsp;</div></div></div>');

        }else {
            $("#visitor_list")
                .append('<div class="mcase-wrapper" onclick="createChatEntry(\'' + userList[i].id + '\',\'' + userList[i].firstname + '\');"><div style="height: 50px; position: relative;"><div class="mprice-wrapper" ><span></span></div><div class="morder-wrapper"><div class="order"><span > ' + userList[i].firstname + ' ' + userList[i].lastname + ' </span></div></div><div class="mbottom-line">&nbsp;</div></div></div>');
        }
    }
}


/*2.Function Name : - iOSContextCreated()
   Paremeters    : - No
   Perpose       : - Fatch OMX data use 'case' table    
   Developer: - DiroLabs
*/

function getAllEntriesForTableCase() {
    var table = "case";
    var date = "";
    var semi = "";
    var orderby = "desc";
    var isshare = 1;
    var skipRow = 0;
    //for getting all rows
    var rowLimit = 0;
    var data = getAllObjectData(table, skipRow, rowLimit, orderby, date, semi, isshare);

    var userList = JSON.parse(data);

    for (var i = 0; i < userList.length; i++) {
        var logo = profilePic(userList[i].data.mobile);
        if (i == 0) {
            $("#case_list")
                .html('<div class="mcase-wrapper" onclick="chat(\'' + userList[i].dataId + '\',\'' + userList[i].data.receivers + '\',\'' + userList[i].data.casename + '\')"><div style="height: 50px; position: relative;"><div class="mprice-wrapper" ><span><img src="images/'+logo+'"  class="user-logo" /></span></div><div class="morder-wrapper"><div class="order"><span > ' + userList[i].data.casename + ' </span></div></div><div class="mbottom-line">&nbsp;</div></div></div>');
        } else {
            $("#case_list")
                .append('<div class="mcase-wrapper" onclick="chat(\'' + userList[i].dataId + '\',\'' + userList[i].data.receivers + '\',\'' + userList[i].data.casename + '\')"><div style="height: 50px; position: relative;"><div class="mprice-wrapper" ><span><img src="images/'+logo+'"  class="user-logo" /></span></div><div class="morder-wrapper"><div class="order"><span > ' + userList[i].data.casename + ' </span></div></div><div class="mbottom-line">&nbsp;</div></div></div>');
        }
    }
}



/*3. Function Name : - chatSend()
   Paremeters    : - No
   Perpose       : - Send Chat OXM data Using 'caseid' table     
   Developer: - DiroLabs */
function chatSend() {
    var message = document.getElementById("chatmsg").value;
    if (message != '') {
        var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + mobile + '.jpg","chattime":"' + formatAMPM() + '","type":"msg","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '"}';
        var ret = setObjectData(messageJson, localStorage.getItem("caseid"), localStorage.getItem("receivers"));
        document.getElementById("chatmsg").value = '';
    } else {
        doMessageToast('Type your text...');
    }
}
/*4. Function Name : - createChatEntry()
   Paremeters    : - No
   Perpose       : - Create Omx Using 'case' table     
   Developer: - DiroLabs
*/
function createChatEntry(visitorId, visitorName) {
    var data = '{"casename":"' + visitorName + '","caseowner":"' + name + '","mobile":"' + mobile + '","createtime":"' + epocTime() + '","receivers":"' + visitorId + '","status":"1"}';
    var rowId = setObjectData(data, "case", visitorId);
    if (rowId != "") {
        localStorage.setItem("caseid", rowId);
        localStorage.setItem("receivers", visitorId);
        location.href = "chat.html";
    }
}

/*5. Function Name : - chat()
   Paremeters    : - 1.id:- table key we use this key as a case id, 2.vst:- oxm receivers list
   Perpose       : - Set caseid & receivers get table chat data and redirect on chat page     
   Developer: - DiroLabs
 */
function chat(id, vst, casename) {
    localStorage.setItem("caseid", id);
    localStorage.setItem("receivers", vst);
    localStorage.setItem("casename", casename);
    location.href = "chat.html";
    /*$("#name").html(casename);*/
}

/*6. Function Name : - showChatList()
   Paremeters    : - No
   Perpose       : - display chat data   
   Developer: - DiroLabs
*/
function showChatList() {
    $("#name").html(localStorage.getItem("casename"));
    var table = localStorage.getItem("caseid");
    var date = "";
    var semi = "";
    var orderby = "asc";
    var isshare = 1;
    var data = getAllObjectData(table, 0, 0, orderby, date, semi, isshare);

    var userList = JSON.parse(data);
    for (var i = 0; i < userList.length; i++) {

        if (mobile == userList[i].data.mobile) {
            if (userList[i].data.type == "msg") {
                $("#chat")
                    .append('<li class="self" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/HYcn9xO.png" draggable="false"/></div><div class="msg"><p>' + userList[i].data.message + '</p><time>' + userList[i].data.chattime + '</time></div></li>');
            } else {
                $("#chat")
                    .append('<li class="self" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/HYcn9xO.png" draggable="false"/></div><div class="msg"><p><img src="multimedia/' + userList[i].data.filename + '" /></p><time>' + userList[i].data.chattime + '</time></div></li>');
            }

        } else {

            if (userList[i].data.type == "msg") {
                $("#chat")
                    .append('<li class="other" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/DY6gND0.png" draggable="false"/></div><div class="msg"><p>' + userList[i].data.message + '</p><time>' + userList[i].data.chattime + '</time></div></li>');
            } else {

                $("#chat")
                    .append('<li class="other" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/DY6gND0.png" draggable="false"/></div><div class="msg"><p><img src="multimedia/' + userList[i].data.filename + '" /></p><time>' + userList[i].data.chattime + '</time></div></li>');
            }
        }
    }
}
/*7. Function Name : - deltaChanges()
   Paremeters    : -  2.data:- return chat dataon  delta changes 
   Perpose       : - display chat data   
   Developer: - DiroLabs*/
function deltaChanges(data) {

    var userList = JSON.parse(data);
    for (var i = 0; i < userList.length; i++) {
        if (mobile == userList[i].data.mobile) {
            if (userList[i].data.type == "msg") {
                $("#chat")
                    .append('<li class="self" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/HYcn9xO.png" draggable="false"/></div><div class="msg"><p>' + userList[i].data.message + '</p><time>' + userList[i].data.chattime + '</time></div></li>');
            } else {
                $("#chat")
                    .append('<li class="self" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/HYcn9xO.png" draggable="false"/></div><div class="msg"><p><img src="multimedia/' + userList[i].data.filename + '" /></p><time>' + userList[i].data.chattime + '</time></div></li>');
            }

        } else {
            if (userList[i].data.type == "msg") {
                $("#chat")
                    .append('<li class="other" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/DY6gND0.png" draggable="false"/></div><div class="msg"><p>' + userList[i].data.message + '</p><time>' + userList[i].data.chattime + '</time></div></li>');
            } else {

                $("#chat")
                    .append('<li class="other" id="' + userList[i].dataId + '"><div class="avatar"><img src="http://i.imgur.com/DY6gND0.png" draggable="false"/></div><div class="msg"><p><img src="multimedia/' + userList[i].data.filename + '" /></p><time>' + userList[i].data.chattime + '</time></div></li>');
            }
        }
    }
}

/*8. Function Name : - capture()
   Paremeters    : - No
   Perpose       : - capture image using gallary and camera   
   Developer: - DiroLabs
*/
function capture() {
    var filename = epocTime() + ".jpg";
    captureImage(filename);
}
/*9. Function Name : - success()
   Paremeters    : - 1.filename :- return capture fie name
   Perpose       : - capture return callback function   
   Developer: - DiroLabs
 */
function success(filename) {
    var file = JSON.parse(filename);
    var message = "image";
    var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + file[1] + '","chattime":"' + formatAMPM() + '","type":"img","createTime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '"}';
    var ret = setObjectAttachmentData(localStorage.getItem("caseid"), file[1], messageJson, localStorage.getItem("receivers"));
}
/*10. Function Name : - failure()
   Paremeters    : - 1.msg :- return error message in case user cancal capture pic
   Perpose       : - return error messge   
   Developer: - DiroLabs
*/
function failure(msg) {
    document.write("fail" + msg);
}

function onBack() {
    doPageBack();
}

/*11.Function Name : - formatAMPM()
    Paremeters    : - no
    Perpose       : - Convert date to AM and PM Formate 
    Developer: - DiroLabs
*/
function formatAMPM() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

/*12.Function Name : - epocTime()
   Paremeters    : - no
   Perpose       : - Convert date to epoc milliseconds Formate 
   Developer: - DiroLabs
 */
function epocTime() {
    var d = new Date();
    var n = d.getTime();
    return n;
}

function profilePic(mobile){

  var visitorData = getVisitorInfo(mobile);
  var visitorObj = JSON.parse(visitorData);
  return(visitorObj.profilepic);
}

function aboutApp(){

var data = getData("appinfo",null);
var appData = JSON.parse(data);

  $("#appname").html(appData[0].appname);
  $("#appowner").html(appData[0].appowner);
  $("#appversion").html(appData[0].appversion);
  $("#updateddate").html(appData[0].updateddate);

}





