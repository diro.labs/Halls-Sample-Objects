var app = angular.module('slidebox', ['ionic', 'tabSlideBox'])
    .run(['$q', '$http', '$rootScope', '$location', '$window', '$timeout',
        function ($q, $http, $rootScope, $location, $window, $timeout) {}
    ]);

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('index', {
        url: '/',
        templateUrl: 'index.html',
        controller: 'IndexCtrl'
    });

    $stateProvider.state('chatmng', {
        url: '/chatmng',
        templateUrl: 'chatmng.html',
        controller: 'chatmngCtrl'
    });

    $urlRouterProvider.otherwise("/");
});

app.controller("IndexCtrl", ['$rootScope', "$scope", "$ionicPopover", "$stateParams", "$q", "$location", "$window", '$timeout', 'myservice',
    function ($rootScope, $scope, $ionicPopover, $stateParams, $q, $location, $window, $timeout, BackButton, myservice) {
        $scope.myservice = myservice;
        $scope.platform = ionic.Platform.platform();
        $scope.quantity = 0;
        $scope.yesterday = false;
        $scope.noactive = false;
        $scope.mobile = "";
        $scope.name = "";
        $scope.visitorid = "";

        $scope.goBack = function () {
            doPageBack();
        }
        $scope.goHome = function () {
            doPageHome();
        }
        $scope.createCase = function () {

            var list = getVisitorList();
            var userList = JSON.parse(list);
            var managerList = [];
            for (var i = 0; i < userList.length; i++) {
                if (userList[i].owner == 1) {
                    managerList.push(userList[i].id);
                }
                managerList.push($scope.visitorid);
            }
            var filter = "case";
            var groupname = $("#groupname")
                .val();
            var data = '{"casename":"' + $scope.name + '","caseowner":"' + $scope.name + '","mobile":"' + $scope.mobile + '","createtime":"' + epocTime() + '","receivers":"' + managerList + '","status":"1"}';
            var caseid = setObjectData(data, filter, String(managerList));

        }

        $scope.active = function () {
            $window.location.href = 'index-visitor.html';
        }
        $scope.complete = function () {
            $window.location.href = '#/complete';
        }
        $timeout(function () {
            iOSContextCreated();
        }, 50);

        $timeout(function () {
            hideQviki();
            var userData = getUserDetail();
            var userObjectJS = JSON.parse(userData);
            $scope.mobile = userObjectJS[0].mobile;
            $scope.name = userObjectJS[0].firstName;
            $scope.visitorid = userObjectJS[0].visitorid;

            var filter = "case";
            var date = "";
            var semi = "status|1";
            var orderby = "desc";
            var isshare = 1;
            var data = getAllObjectData(filter, 0, 0, orderby, date, semi, "1");
            if (data == '' || data == null || data == "null") {
                $scope.noactive = true;
            }


            // doMessageToast(data);     
            $scope.activeOrders = JSON.parse(data);

            var filter = "case";
            var date = "";
            var semi = "status|0";
            var orderby = "desc";
            var isshare = 1;
            var dataCompleteOrder = getAllObjectData(filter, 0, 0, orderby, date, semi, "1");
            $scope.completeOrders = JSON.parse(dataCompleteOrder);

        }, 200);

        $scope.loadComleteOrders = function () {
            var filter = "case";
            var date = "";
            var semi = "status|0";
            var orderby = "desc";
            var isshare = 1;
            var dataCompleteOrder = getAllObjectData(filter, 0, 0, orderby, date, semi, "1");
            $scope.completeOrders = JSON.parse(dataCompleteOrder);
        }
        $scope.loadchat = function (caseid, receivers) {
            window.localStorage['caseid'] = caseid;
            window.localStorage['receivers'] = receivers;
            $window.location.href = '#/chatmng';
        }
        $scope.get_Tag = function (data) {
            var count = 0;
            var titleName = '';
            for (var j = 0; j < data.orderitem.length; j++) {
                titleName = data.orderitem[0].quantity + " " + data.orderitem[0].itemname;
                count++;
            }
            if (count < 2) {
                return titleName;
            } else {
                return titleName + " +" + (count - 1);
            }

        }
        $scope.getBill = function (data) {
            var bill = 0;
            for (var j = 0; j < data.orderitem.length; j++) {
                bill += data.orderitem[j].quantity * data.orderitem[j].itemprice;
            }
            return bill;
        }

        $scope.loadPageNg = function (caseIdv, isComplete) {

            hello(caseIdv, isComplete);
            $window.location.href = '#/chatmng';
        }

        $scope.newCase = function () {
            hello(1, 0);
            $window.location.href = '#/chatmng';
        }



        $scope.orderTime = function (date) {
            return getDifferenceFromCurrentTime(date.toString());
        }
        $scope.orderTime1 = function (date) {

            return getDifferenceFromCurrentTime(date.toString());
        }

        $scope.getCaseCount = function (caseid) {
            return getCaseCount(caseid);
        }
        $scope.epocTotime = function (epoc) {
            // doMessageToast(epoc);
            var temp = parseInt(epoc);
            var date = new Date(temp);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
            //return getDifferenceFromCurrentTime(date);
        }
        $scope.todayOrderis = function (date) {
            var today = '';
            var d = new Date();
            var toDate = d.getDate();
            var toMonth = d.getMonth();
            var pdate = new Date(parseInt(date));
            var date = pdate.getDate();
            var month = pdate.getMonth();
            if (toDate == date && toMonth == month) {
                today = true;
            } else {
                today = false;
            }
            return today;
        }

        $scope.todayOrderNot = function (date) {
            if (date != undefined) {
                var today = '';
                var d = new Date();
                var toDate = d.getDate();
                var toMonth = d.getMonth();
                var date1 = new Date(parseInt(date));
                var date = date1.getDate();
                var month = date1.getMonth();
                if (toDate == date && toMonth == month) {
                    today = false;
                } else {
                    today = true;
                    $scope.yesterday = true;
                }
                return today;
            } else {
                return false;
            }
        }
    }
])



app.controller("chatmngCtrl", ['$rootScope', "$scope", "$ionicPopover", "$ionicModal", "$stateParams", "$q", "$location", "$window", '$timeout', 'myservice', '$ionicScrollDelegate',
    function ($rootScope, $scope, $ionicPopover, $ionicModal, $stateParams, $q, $location, $window, $timeout, myservice, $ionicScrollDelegate) {

        $scope.platform = ionic.Platform.platform();
        $scope.myservice = myservice;
        $scope.title = '';
        $scope.noOfItem = '';
        $scope.isComplete = 0;

        $scope.$on('$ionicView.loaded', loadView);

        function loadView() {
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('homeScroll')
                    .scrollBottom();

            }, 0);
        }

        $scope.loadViewBottom = function () {
            $ionicScrollDelegate.$getByHandle('homeScroll')
                .scrollBottom();
        }

        $timeout(function () {

            var filter = localStorage.getItem("caseid");
            var receivers = localStorage.getItem("receivers");

            var date = "";
            var semi = "";
            var orderby = "asc";
            var isshare = 1;
            var data = getAllObjectData(filter, 0, 0, orderby, semi, date, "1"); /*(filter,skip,limit,orderBy,daterange,samifilter,isshare) */
            //doMessageToast(data);
            document.write(data);
            chatView(data);


            var itemName = "";
            var titleName = '';
            $scope.title = "Qviki To Manger";
            $scope.isComplete = document.getElementById("isComplete")
                .value;
            if ($scope.isComplete == 1) {
                hideQviki();
            } else {
                showQviki();
            }
            var caseid = document.getElementById("caseid")
                .value;
            var caseDataObj = document.getElementById("casedata-" + caseid)
                .value;
            var caseData = JSON.parse(caseDataObj);

            var count = 0;
            for (var j = 0; j < caseData.orderitem.length; j++) {
                itemName += ", " + caseData.orderitem[j].itemname;
                count++;
            }

            $scope.noOfItem = count;

            if (itemName == '') {
                titleName = "Qviki To Manger";
            } else {
                titleName = caseData.orderitem[0].itemname;
            }
            $scope.title = titleName;
        }, 10);

        //$scope.name = UserService.name;
        $scope.focusedHide = true;
        $scope.chatmsg = '';
        $scope.name = '';
        $scope.mobile = '';

        /*var dd = getUserDetailJs();*/
        var dd = getUserDetail();
        userObjectJS = JSON.parse(dd);
        $scope.mobile = userObjectJS[0].mobile;
        $scope.name = userObjectJS[0].firstName;

        // greeting.ChangeText('hello');
        $scope.goBack = function () {
            doPageBack();
        }
        $scope.goHome = function () {
            doPageHome();
        }
        $scope.active = function () {
            $window.location.href = 'index-visitor.html';
        }
        //getCaseId();

        $scope.chatBoxFocus = function () {
            $scope.focusedHide = false;
            $scope.focusedShow = true;
        }
        $scope.chatBoxBlur = function () {
            $scope.focusedHide = true;
            $scope.focusedShow = false;
        }
        $scope.touchStart = function () {
            onAttchemntAudio();
        }
        $scope.touchEnd = function () {
            audioRecordStop();
        }
        $scope.sendChatData = function () {
            var message = document.getElementById("chatmsg")
                .value;
            if (message != '') {
                datetime = formatAMPM();
                createTime = epocTime();
                var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","type":"msg","createTime":"' + createTime + '","receivers":"' + localStorage.getItem("receivers") + '" }';
                var ret = setObjectData(messageJson, localStorage.getItem("caseid"), localStorage.getItem("receivers"));

                document.getElementById("chatmsg")
                    .value = '';
            }
        }

        $scope.updateCustomRequest = function (data) {
            $scope.chatData = JSON.parse(data);

        }

        $scope.stripTitle = function (title) {
            return title.substring(0, 18);
        }
        $scope.canceledOrder = function () {
            var caseid = document.getElementById("caseid")
                .value;
            createTime = epocTime();
            datetime = formatAMPM();
            var message = "Your Order has been canceled";
            var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","type":"msg","createTime":"' + createTime + '"}';
            var messageString = JSON.parse(messageJson);
            setData("callFromActivity", "msg", messageJson, null);
            $scope.popover.hide();
        }
        $scope.response = function (msg) {

            var message = msg;
            datetime = formatAMPM();
            createTime = epocTime();
            var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"test.me","chattime":"' + datetime + '","type":"msg","createTime":"' + createTime + '"}';
            var messageString = JSON.parse(messageJson);
            setData("callFromActivity", "msg", messageJson, null);
            $scope.modal.hide();
            $scope.popover.hide();
        }

        $scope.onAttchemnt = function () {

            var d = new Date();
            var uniqueName = "audio" + d.getTime() + ".pcm";
            var message = "Voice Message";
            datetime = formatAMPM();
            createTime = epocTime();
            var messageJson = '{"message":"' + message + '" , "messageFrom":"' + $scope.name + '","mobile":"' + $scope.mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"audio","createTime":"' + createTime + '"}';
            var messageString = JSON.parse(messageJson);
            getGalleryOpen("msg", uniqueName, "audio/pcm", "callFromActivity", messageJson);

        }

        $ionicPopover.fromTemplateUrl('popover.html', {
                scope: $scope,
                animation: $scope.animation
            })
            .then(function (popover) {
                $scope.popover = popover;
            });

        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };

        $scope.closePopover = function () {
            $scope.popover.hide();
        }

        $ionicModal.fromTemplateUrl('modal.html', {
                scope: $scope
            })
            .then(function (modal) {
                $scope.modal = modal;
            });


    }
])

app.controller("headerCtrl", ['$rootScope', "$scope", "$ionicPopover", "$stateParams", "$q", "$location", "$window", '$timeout',
        function ($rootScope, $scope, $ionicPopover, $stateParams, $q, $location, $window, $timeout) {

            $scope.greeting = greeting;
            $scope.goBack = function () {
                doPageBack();
            }

            $scope.goHome = function () {
                doPageHome();
            }

        }
    ])

    .service('myservice', function () {
        this.xxx = 0;
        this.sayHello = function (text) {
            this.xxx = text;
        };
    })


    .directive('myTouchstart', [function () {
        return function (scope, element, attr) {

            element.on('touchstart', function (event) {
                scope.$apply(function () {
                    scope.$eval(attr.myTouchstart);
                });
            });
        };
    }])

    .directive('myTouchend', [function () {
        return function (scope, element, attr) {

            element.on('touchend', function (event) {
                scope.$apply(function () {
                    scope.$eval(attr.myTouchend);
                });
            });
        };
    }])

    .directive('fallbackSrc', function () {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this)
                        .attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    });


function hello(id, isComplete) {
    document.getElementById("caseid")
        .value = id;
    document.getElementById("isComplete")
        .value = isComplete;
}

function getCaseId() {

    var id = document.getElementById("caseid")
        .value;
    if (id == 1 || id == '1') {
        var data = getCaseAll(null);
        callFromActivity(data);

    } else {
        var data = getCaseAll(id);
        callFromActivity(data);
    }
}

function callFromActivity(data) {
    chatView(data);
}


function deltaChanges(data) {
    chatView(data);
}

function chatView(data) {

    var dd = getUserDetail();
    userObjectJS = JSON.parse(dd);
    mobile = userObjectJS[0].mobile;
    name = userObjectJS[0].firstName;
    var ismanager = userObjectJS[0].ismanager;
    var logo = '';
    var obj = JSON.parse(data);
    var logo = 'user.jpg';
    var logov = 'muser.jpg';

    for (var i = 0; i < obj.length; i++) {

        var visitorData = getVisitorInfo(obj[i].data.mobile);
        if (visitorData == "null") {
            var logo = "user.jpg";
            logov = "muser.jpg";
        } else {
            var visitorObj = JSON.parse(visitorData);
            var logo = visitorObj.profilepic;
            var logov = visitorObj.profilepic;
            if (logo == '' || logo == null) {
                var logo = "user.jpg";
                logov = "muser.jpg";
            }
        }

        var logo_id = Math.random();

        if (mobile == obj[i].data.mobile) {
            if (obj[i].data.type == 'msg') {
                $("#chat_list")
                    .append('<div class="right-chet-wrepper" ><div class="chat-box"><div class="chat"> ' + obj[i].data.message + ' </div></div><div class="triangle-right"></div><div class="user-img" id="logo' + logo_id + '"><img src="images/' + logo + '"  class="user-logo-right" onerror="defailtLogo(' + logo_id + ');" /></div><div class="date-box"> ' + obj[i].data.chattime + ' <i class="icon d-f-a-0-ic-andr-tick-done"></i></div></div>');
            }
            if (obj[i].data.type == 'feed') {
                if (obj[i].data.feedbackstatus == 1) {
                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }


                }
                if (obj[i].data.feedbackstatus == 2) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 3) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 4) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
                if (obj[i].data.feedbackstatus == 5) {

                    if (obj[i].data.feedbacktext != "") {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star5"></i></a></div><div class="text" style="margin-bottom:5px;"><span class="comment">Comment </span>: ' + obj[i].data.feedbacktext + '</div></div><div style="clear:both;"></div><br/>');
                    } else {
                        $("#chat_list")
                            .append('<div class="feedback-wrepper" id="' + obj[i].uid + '" style="height: auto;"><div class="text"> Feedback Received </div><div class="rating-box"><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star1"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star2"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star3"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star4"></i></a><a href="#"><i class="icon d-f-a-0-ico-andr-star-field star-black" id="star5"></i></a></div></div><div style="clear:both;"></div><br/>');
                    }
                }
            }
        } else {
            if (obj[i].data.type == 'msg') {
                $("#chat_list")
                    .append('<div class="left-chet-wrepper"><div class="user-img" id="logo' + logo_id + '"><img src="images/' + logov + '" class="user-logo-left"  onerror="defailtLogo(' + logo_id + ');" /></div><div class="triangle-left"></div><div class="chat-box">' + obj[i].data.message + '</div><div class="date-box"> ' + obj[i].data.chattime + ' <i class="icon d-f-i-1-ic-andr-double-tick-done"></i></div></div>');
            }

            if (obj[i].data.type == 'feed') {
                doMessageToast("data id:-" + obj[i].dataId);
                var one = 1;
                var two = 2;
                var three = 3;
                var four = 4;
                var five = 5;
                $("#chat_list")
                    .append('<div class="feedback-wrepper" id="' + obj[i].uid + '"><div class="text"> We need your feedback </div><div class="rating-box"><a href="#" onclick="submitFeedBackDocument(' + one + ');"><i class="icon d-f-a-0-ico-andr-star-field " id="star1"></i></a><a href="#" onclick="submitFeedBackDocument(' + two + ');"><i class="icon d-f-a-0-ico-andr-star-field" id="star2"></i></a><a href="#" onclick="submitFeedBackDocument(' + three + ');"><i class="icon d-f-a-0-ico-andr-star-field" id="star3"></i></a><a href="#" onclick="submitFeedBackDocument(' + four + ');"><i class="icon d-f-a-0-ico-andr-star-field" id="star4"></i></a><a href="#" onclick="submitFeedBackDocument(' + five + ');"><i class="icon d-f-a-0-ico-andr-star-field" id="star5"></i></a></div><div class="feedback"><textarea  id="feedbacktext"  style=""></textarea><input type="button" id="submit" value="SUBMIT" class="submit" onclick="submitFeedBack(\'' + obj[i].dataId + '\')"></div></div><div style="clear:both;"></div><br/><br/><br/><br/>');
            }
        }
    }
}

function defailtLogo(id) { //showToastPlateform("data"+id); 
    $("#logo" + id)
        .html('<img src="images/user.jpg"  class="user-logo-right" />');
}

function onAttchemntAudio() {
    var d = new Date();
    var uniqueName = "audio" + d.getTime() + ".pcm";
    var message = "Voice Message";
    datetime = formatAMPM();
    createTime = epocTime();
    var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"audio","createTime":"' + createTime + '"}';
    var messageString = JSON.parse(messageJson);
    getGalleryOpen("msg", uniqueName, "audio/pcm", "callFromActivity", messageJson);

}

function onAttchemnt() {
    var d = new Date();
    var uniqueName = "image" + d.getTime() + ".jpg";
    var message = "Photo";
    datetime = formatAMPM();
    createTime = epocTime();
    var messageJson = '{"message":"' + message + '" , "messageFrom":"' + name + '","mobile":"' + mobile + '","filename":"' + uniqueName + '","chattime":"' + datetime + '","type":"image","createTime":"' + createTime + '"}';
    var messageString = JSON.parse(messageJson);
    getGalleryOpen("msg", uniqueName, "image/jpeg", "callFromActivity", messageJson);
}


function iOSContextCreated() {


}