/*__________________     JS GLOBAL VARIBLES  _____________*/
/*var grandTotal=[];
var tagData = [];
var searchObj = ''; 
var caseid="";
var feedbackStatus = 0;
    orderStatus = 0;*/
var feedbackStatus = 0;

'use strict';

angular.module('tabSlideBox', [])
.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});
/*__________________     JS FUNCTION FOR SUBMIT CHAT DATA  _____________*/
function getDateTime(){
  var now = new Date();
  var strDateTime = [[AddZero(now.getHours()), AddZero(now.getMinutes())].join(":"), now.getHours() >= 12 ? "PM" : "AM"].join(" ");
    return strDateTime;
}


/*__________________     JS FUNCTION FOR GENERAL PERPOSE LIKE FIND SPECIFIC DATA/FILE EXTENSTION  _____________*/

jQuery.fn.exists = function(){return ($(this).length > 0);}
function changeColorlike(id){ document.getElementById(id).style.color = "green"; }
function changeColordislike(id){ document.getElementById(id).style.color = "red"; }
function changeColorsmile(id){ document.getElementById(id).style.color = "yellow"; }
function audioPlay(filename,imgid){ 
  //audioPlayPlateform(filename); 
  //doMessageToast(imgid);
  document.getElementById("audioimg"+imgid).style.display = "block";
  document.getElementById("line"+imgid).style.marginTop = "5px";

  var duration = doAudioRecordingPlay(filename);
  //doMessageToast(duration);
  setInterval(function(){ 
     document.getElementById("audioimg"+imgid).style.display = "none";
     document.getElementById("line"+imgid).style.marginTop = "30px";

  }, parseInt(duration));
}
function audioStop(filename,i){  
 //audioStopJs();
 doAudioStop();
}
function audioRecordStop(){  
  doAudioRecordingStop();
}
function imageOpen(filename){ 
  doFullScreenImage(filename); 
}
function formatAMPM() {
  var date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}



function epocTime(){ var d = new Date(); var n = d.getTime(); return n; }
/*__________________     JS FUNCTION FOR VIEW DATA  _____________*/

function  submitFeedBack(dataid){
 doMessageToast(dataid);
  var dd = getUserDetail();
  userObjectJS = JSON.parse(dd);
  mobile = userObjectJS[0].mobile;
  name = userObjectJS[0].firstName;
 
  var feedback = document.getElementById("feedbackRating").value;
  var feedbacktext = document.getElementById("feedbacktext").value;
  
  
  var messageJson='{"message":"Feedback","messageFrom":"'+ name +'","caseid":"'+ dataid +'" , "feedbackstatus":"'+ feedback +'","feedbacktext":"'+ feedbacktext +'","mobile":"'+ mobile +'","datetime":"'+ getDateTime() +'","status":"0","type":"feed","createTime":"'+ epocTime() +'","receivers":"'+localStorage.getItem("receivers")+'" }';

   var updata = updateObjectData(dataid, messageJson); 
 /* doMessageToast("Your feedback status submit"+ updata);*/
 
  if(feedback == 1){ doMessageToast("Thank you, we will connect with you shortly");
  }
  if(feedback == 2 || feedback == 3){ doMessageToast("Thank you, we will connect with you to improve our service");
  }
  if(feedback == 4 || feedback == 5){ doMessageToast("Thank you");
  }

  var getdata = getObjectData(localStorage.getItem("caseid"));
  var caseData = JSON.parse(getdata);
  var filter = "case";
  var data = '{"casename":"' + caseData[0].casename + '","caseowner":"' + caseData[0].casename + '","mobile":"' + caseData[0].mobile + '","createtime":"' + epocTime() + '","receivers":"' + localStorage.getItem("receivers") + '","status":"0","orderitem":'+jsonObjString+',"orderstatus":'+orderstatus+',"orderduration":'+maxTime+',"orderdatetime":"'+dt+'"}';
  var updata = updateObjectData(localStorage.getItem("caseid"), data);  
  doPageHome();  
}



function submitFeedBackDocument(feedback)
{
  document.getElementById("feedbackRating").value = feedback;
  if(feedback == 1){ $("#star1").addClass("star-black");  $("#star2").removeClass("star-black");$("#star3").removeClass("star-black");
    $("#star4").removeClass("star-black");$("#star5").removeClass("star-black");
  } 
  if(feedback == 2){ $("#star1").addClass("star-black"); $("#star2").addClass("star-black");  $("#star3").removeClass("star-black");
    $("#star4").removeClass("star-black");$("#star5").removeClass("star-black");
  }
  if(feedback == 3){
    $("#star1").addClass("star-black"); $("#star2").addClass("star-black"); $("#star3").addClass("star-black"); $("#star4").removeClass("star-black"); $("#star5").removeClass("star-black");
  }
  if(feedback == 4){
    $("#star1").addClass("star-black"); $("#star2").addClass("star-black"); $("#star3").addClass("star-black"); $("#star4").addClass("star-black"); $("#star5").removeClass("star-black");
  }
  if(feedback == 5){
    $("#star1").addClass("star-black"); $("#star2").addClass("star-black"); $("#star3").addClass("star-black"); $("#star4").addClass("star-black"); $("#star5").addClass("star-black");
  }

  $("#submit").addClass("submit-change-color");
}



function setFeedback(){
  doMessageToast("call");
 /* $("#footer").hide(); $("#tag-btn").hide();*/
  var dd = getUserDetail();
  userObjectJS = JSON.parse(dd);
  mobile = userObjectJS[0].mobile;
  name = userObjectJS[0].firstName;
  if(feedbackStatus == 0){
  
    var message="Your order delivered successfully";
   
    var oxmData ='{"message":"'+message+'" , "messageFrom":"'+ name +'","mobile":"'+ mobile +'","filename":"test.me","chattime":"'+ formatAMPM() +'","type":"msg","createTime":"'+epocTime() +'","receivers":"'+localStorage.getItem("receivers")+'" }';
         
    var ret = setObjectData(oxmData, localStorage.getItem("caseid"), localStorage.getItem("receivers"));
   
    var data='{"message":"Feedback","messageFrom":"'+name+'","caseid":"'+ localStorage.getItem("caseid") +'" , "feedbackstatus":"none","mobile":"'+mobile+'","datetime":"'+ formatAMPM() +'","status":"1","type":"feed","createTime":"'+ epocTime() +'","receivers":"'+localStorage.getItem("receivers")+'" }';

    var ret1 = setObjectData(data, localStorage.getItem("caseid"), localStorage.getItem("receivers"));
    feedbackStatus = 1;
  }
}

function AddZero(num){ return (num >= 0 && num < 10) ? "0" + num : num + ""; }

/*__________________     JS FUNCTION FOR TAG DATA  _____________*/

function getTempData(){  }
function tag(tagID,tagName, price,quantity,isSelected,duration) {
  this.tagID = tagID;
  this.tagName = tagName;
  this.price = price;
  this.quantity = quantity;
  this.isSelected = isSelected;
  this.duration = duration;
}
  
function createTag(data){

  $("#tagData").val(data);
}
   
function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
     results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function search_tag(){
  var str = $("#search111").val();
  var obj  =JSON.parse(searchObj);
  var count = 0; var zero = 0.00;
    
  for (var i = 0; i < obj.length; i++)
  {
    $("#spanTotalAmount").html(zero);
      grandTotal[i]=0;
      tagData[i] = new tag(obj[i].itemid, obj[i].itemname, obj[i].itemprice, "0","0","0");
    var c1 = "c_"+i;
    var matchStr  = obj[i].itemname.search(str);
    if( matchStr != -1)
    {
      if(count == 0)
      {

      $("#taglist").html('<div class="tag-wrapper"><div class="tag-left"><label class="item-checkbox checkbox tag-check" ><input type="checkbox" id="'+c1+'" onclick="isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\');"></label><div  class="tag-name">' +obj[i].itemname+ '</div><div class="price"> RS. ' +obj[i].itemprice+ '</div></div><div class="tag-right"><div class="available">Available</div><div class="quantity"><button class="icon ion-minus circle border-none" id="minus" onClick = "doMinus(\'' +c1+ '\',\''+obj[i].itemid+'\',\''+obj[i].itemprice+'\',\''+i+'\'); isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\');"></button><input type="text" placeholder="0" id="'+obj[i].itemid+'" value="0"  class="quantity-show" readonly><button class="icon ion-plus circle border-none"  id="plus" onClick = "doPlus(\'' +c1+ '\',\''+obj[i].itemid+'\',\''+obj[i].itemprice+'\',\''+i+'\'); isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\')"></button></div></div></div>');
        count++;
      }
      else
      {
    
      $("#taglist").append('<div class="tag-wrapper"><div class="tag-left"><label class="item-checkbox checkbox tag-check" ><input type="checkbox" id="'+c1+'" onclick="isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\');"></label><div  class="tag-name">' +obj[i].itemname+ '</div><div class="price"> RS. ' +obj[i].itemprice+ '</div></div><div class="tag-right"><div class="available">Available</div><div class="quantity"><button class="icon ion-minus circle border-none" id="minus" onClick = "doMinus(\'' +c1+ '\',\''+obj[i].itemid+'\',\''+obj[i].itemprice+'\',\''+i+'\'); isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\');"></button><input type="text" placeholder="0" id="'+obj[i].itemid+'" value="0"  class="quantity-show" readonly><button class="icon ion-plus circle border-none"  id="plus" onClick = "doPlus(\'' +c1+ '\',\''+obj[i].itemid+'\',\''+obj[i].itemprice+'\',\''+i+'\'); isTagItemSelected(\'' +c1+ '\',\'' +i+ '\',\'' +obj[i].itemid+ '\',\'' +obj[i].itemname+ '\',\'' +obj[i].itemprice+ '\',\'' +obj[i].duration+ '\')"></button></div></div></div>');
      }
    }
  }
}   

function clearSearch(){
   $("#search111").val(null);
}
function hideQviki(){
  doHideQuikiButton();
}

function showQviki(){
  doShowQuikiButton();
}



/*function addData(obj){
   var caseData =  JSON.parse(obj);
   var caseid = document.getElementById("caseid").value;
  document.getElementById("casedata-"+caseid).value = caseData;
}*/